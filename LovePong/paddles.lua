Object = require "classic"
CPaddles = Object:extend()


function CPaddles:new(_x,_y,_isPlayer)
  self.x = _x or 0
  self.y = _y or 0
  self.w = 10 
  self.h = 50
  self.speed = 200
  self.isPlayer = _isPlayer
end

function CPaddles:Update(dt, ball)
  if (self.isPlayer) then
   if love.keyboard.isDown("down") then
       self.y = self.y + self.speed* dt
    end
    if love.keyboard.isDown("up") then
       self.y = self.y - self.speed* dt
    end
  else
    if(self.y +self.h/2 < ball.y) then
      self.y = self.y + (self.speed * 2.5 * dt)
      end
    if(self.y +self.h/2 > ball.y) then
      self.y = self.y - (self.speed * 2.5 * dt)
      end
  end
  


end

function CPaddles:draw()
  love.graphics.rectangle("fill", self.x, self.y, self.w, self.h)
end

return CPaddles
