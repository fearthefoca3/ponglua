
 Object = require "classic"
 CBall = Object:extend()

local ballSpeed = 400
local screenW = 800
local screenH = 600
function CBall:new()
  self.x = screenW/2
  self.y = screenH/2
  self.w = 10
  self.h = 10
  self.ballAngle = math.rad(20)
  self.speed = ballSpeed
end

function CBall:Update(dt)
  self.x = self.x + math.cos(self.ballAngle) * self.speed * dt
  self.y = self.y + math.sin(self.ballAngle) * self.speed * dt
  
  if(self.y < 0) then
    self.ballAngle = self.ballAngle * -1
  end
  if(self.y > screenH) then
    self.ballAngle = self.ballAngle * -1
  end
      if(self.x < 0) then
    self.x = screenW/2
    self.y = screenH/2
    self.speed = ballSpeed
    
  end
  
  if(self.x > screenW) then
    self.x = screenW/2
    self.y = screenH/2
    self.speed = ballSpeed
  end
  
 
end

function CBall:draw()
  love.graphics.circle("fill", self.x, self.y, self.h, self.w)
end

return CBall

