local  w, h = love.graphics.getDimensions( )
local mouse = {x ,y, w = 10, h = 10 }
local Play = {x = w/2-100 ,y = h*4/5, w = 200, h = 50 }
local marker ={x = 250, y = 80 , w = 10, h = 10}
local Button1 = {x = 320, y = 300 , w = 100, h = 20}
local Button2 = {x = 320, y = 340 , w = 100, h = 20}
local Button3 = {x = 320, y = 380 , w = 100, h = 20}
local index = 1
function collision(a,b)
  if a.x < b.x + b.w and a.x + a.w > b.x and a.y < b.y + b.h and a.y + a.h > b.y then
    return true
    end
end
function love.keypressed( key )
   if key == "down" then
       if(index == 3) then index = 1 else index = index + 1 end
      print(index)
   end
   if key == "up" then
       if(index == 1) then index = 3 else index = index - 1 end
      
   end
   if key == "return" then
     if(index == 1) then --[[ porta a el joc--]] gameStat = 'playing'
     elseif (index == 2) then --[[ porta a el joc pero amb dos players--]]  gameStat = 'gameOver'
    elseif (index == 3) then love.event.quit()  end
  end
  
end

function drawSelection(index) --Aixo es per el menu si apretes les fletxes de pujar o baixar et marca la seleccio
  if ( index == 1) then
    marker.y = 300
  elseif (index == 2) then
    marker.y = 340
  elseif (index == 3) then
    marker.y = 380
  end
end


 Object = require "classic"
 CMainMenu = Object:extend()


function CMainMenu:new()

end

function CMainMenu:Update(dt)
    mouse.x, mouse.y = love.mouse.getPosition( )
  drawSelection(index) -- et pinta el quadradet en diferents posicions depenent del index en que esta
-- aquets tres if son per si cliques amb el ratoli. Hauria e carregar el seguent script
  if (collision(mouse, Button1)) then
     if love.mouse.isDown(1) then
       index = 1
        gameStat = 'playing'
      end
  end
  if (collision(mouse, Button2)) then
     if love.mouse.isDown(1) then
       index = 2
        print("Button2") --- Aixo hauria de carregar algo
      end
  end
  if (collision(mouse, Button3)) then
      if love.mouse.isDown(1)  then
        index = 3
        love.event.quit()
      end
    
  end
  
end

function CMainMenu:draw()
    -- la foto de fons del menu
  love.graphics.draw(love.graphics.newImage("mainmenu.jpg"), 240,70,0,0.75,0.75)
  love.graphics.rectangle("fill", Play.x, Play.y, Play.w, Play.h)
--el marcador de la seleccio. Es el quadradet petit (es podria substituir per una foto de una fletxa
  love.graphics.rectangle("fill", marker.x, marker.y, marker.w, marker.h)
  --love.graphics.setColor(0,0,0,1)

  ---- text del les tres seleccions. la segona encara nose que podem posar pero podria ser mute game o algo aixi
  playGame = love.graphics.newText(love.graphics.newFont("pong.ttf", 20), "Play Game")
  love.graphics.draw(playGame, Button1.x, Button1.y)
  
  Music = love.graphics.newText(love.graphics.newFont("pong.ttf", 20), "Mute/Play Music")
  love.graphics.draw(Music, Button2.x, Button2.y)
  
  ExitGame = love.graphics.newText(love.graphics.newFont("pong.ttf", 20), "Exit Game")
  love.graphics.draw(ExitGame, Button3.x, Button3.y)
end

return CMainMenu



