
 Object = require "classic"
 CGameOver = Object:extend()

local  w, h = love.graphics.getDimensions( )
local mouse = {x , y, w = 10, h = 10 }
local buttonPlayParam = {x = w/2-100 ,y = h*4/5, w = 200, h = 50 }



function CGameOver:new()

end

function CGameOver:Update(dt,ball)
   mouse.x, mouse.y = love.mouse.getPosition( )
  
    if (collision(mouse, buttonPlayParam)) then
        if love.mouse.isDown(1) then
        gameStat = 'playing'
      end
    end
  
end

function CGameOver:draw()
  love.graphics.draw(love.graphics.newImage("gameover.jpg"), 50,50,0,1,1)
  love.graphics.rectangle("line", buttonPlayParam.x, buttonPlayParam.y, buttonPlayParam.w, buttonPlayParam.h)
end

function collision(a,b)
  if a.x < b.x + b.w and a.x + a.w > b.x and a.y < b.y + b.h and a.y + a.h > b.y then
    return true
    end
end


return CGameOver

