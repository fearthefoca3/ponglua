
 Object = require "classic"
 CHud = Object:extend()

local font = love.graphics.newFont("pong.ttf", 60)

function CHud:new()
  self.playerPoints = 0
  self.cpuPoints = 0
end

function CHud:Update(dt,ball)

  if(ball.x < 0) then
    
      self.cpuPoints = self.cpuPoints + 1
      
  end
  
  if(ball.x > 800) then
    self.playerPoints = self.playerPoints + 1
    
  end
  
end

function CHud:draw()
    love.graphics.rectangle("fill", 800/2, 1, 1, 600)
    

    scoreCPU = love.graphics.newText(font, self.cpuPoints)
    love.graphics.draw(scoreCPU, 800*3/4, 50)
    
    scorePlayer = love.graphics.newText(font, self.playerPoints)
    love.graphics.draw(scorePlayer, 800/4, 50)
end

return CHud
