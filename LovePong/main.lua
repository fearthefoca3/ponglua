local w, h -- Variables to store the screen width and height
local score1, socre2
--local ballX, ballY -- Variables to store the position of the ball in the screen (Uncomment at the start of TODO 6)
-----------local ball = {x , y , w = 10, h = 10}
--local ballSpeed -- Variable to store the ball speed (Uncomment at the start of TODO 8)
--local playerX, playerY, cpuX, cpuY -- Variables to store the position of the player and cpu paddle (Uncomment at the start of TODO 10)
--local player = {x, y, w = 20, h = 80}
--local cpu = {x, y, w = 20, h = 80}
--local paddleSpeed -- Variable to store the paddle speed (Uncomment at the start of TODO 12)
--local ballAngle -- Variable to estore the ball movement angle (Uncomment at the start of TODO 16)
 --local playerPoints, cpuPoints -- Variable to store the player and cpu points (Uncomment at the start of TODO 21)
gameStat = 'mainMenu'
 local CBall = CBall or require "ball"
 local CPaddles = CPaddles or require "paddles"
 local CGameOver = CGameOver or require "gameover"
 local CHud = CHud or require "hud"
local CMainMenu = CMainMenu or require "menu"
 allList = { }
 
function collision(a,b)
  if a.x < b.x + b.w and a.x + a.w > b.x and a.y < b.y + b.h and a.y + a.h > b.y then
    return true
    end
  
end
function love.load(arg)
  if arg[#arg] == "-debug" then require("mobdebug").start() end -- Enable the debugging with ZeroBrane Studio
  w, h = love.graphics.getDimensions() -- Get the screen width and height
  gameStat = 'mainMenu' -- 'playing' -- 'gameOver'
    menu = CMainMenu()
    table.insert(allList,menu)
    ball = CBall()
    table.insert(allList,ball)
    Hud = CHud()
    table.insert(allList, Hud)
    ball.speed = ball.speed * -1
    playerPaddle = CPaddles(10, 800/2, true)
    table.insert(allList, playerPaddle)
    cpuPaddle = CPaddles(800-30, 600/2, false)
    table.insert(allList, cpuPaddle)



    gameOver = CGameOver()
    table.insert(allList, gameOver)

  
end

function love.update(dt)

    --ball:Update(dt)
    --playerPaddle:Update(dt,ball)
    --cpuPaddle:Update(dt,ball)

    --gameOver:Update(dt)
    
   --Hud:Update(dt,ball)
   --print(ball.. "hola")
   
   for _,v in ipairs(allList) do
      v:Update(dt,ball)
    end


  if collision(playerPaddle, ball) then
    ball.ballAngle = math.rad(180) - ball.ballAngle
    ball.x =playerPaddle.x +playerPaddle.w+1
    ball.speed = ball.speed +10
  end

  if collision(cpuPaddle, ball) then
    ball.ballAngle = math.rad(180) - ball.ballAngle
    ball.x = cpuPaddle.x - cpuPaddle.w -1
    ball.speed = ball.speed +10
  end

end

function love.draw()
  if(gameStat == 'playing') then
    ball:draw()
    playerPaddle:draw()
    cpuPaddle:draw()
    Hud:draw()
  elseif (gameStat == 'gameOver') then
    gameOver:draw()
elseif(gameStat == 'mainMenu') then
  menu:draw()
  end

end

